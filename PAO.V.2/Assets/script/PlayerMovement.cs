using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private CharacterController m_charCont;

    float m_horizontal;
    float m_vertical;
    public float PlayerSpeed = 0.012f;
    private Camera mainCamera;

    private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        m_charCont = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        mainCamera = FindObjectOfType<Camera>();
    }

    // Update is called once per frame
    void Update()

    {
        if (!PauseMenu.isPaused)
        {
        m_horizontal = Input.GetAxis("Horizontal");
        m_vertical = Input.GetAxis("Vertical");

        if(Input.GetAxis("Horizontal") !=0 && Input.GetAxis("Vertical") !=0)
        {
            PlayerSpeed = 0.0021f;
        }
        else
            PlayerSpeed = 0.03f;

        Vector3 m_playerMovement = new Vector3(m_horizontal, 0f, m_vertical) * PlayerSpeed;

        m_charCont.Move(m_playerMovement);

        if(Input.GetButton("Run"))
        {
            m_charCont.Move(m_playerMovement * 2);
        }

        if(Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
        {
            animator.SetBool("IsWalking", true);
            if(Input.GetButton("Run"))
                animator.SetBool("IsRunning", true);
            else
                animator.SetBool("IsRunning", false);
        }
        else
        {
            animator.SetBool("IsWalking", false);
        }
        //Mouse
        Ray cameraRay = mainCamera.ScreenPointToRay(Input.mousePosition);
        Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
        float rayLenght;


            if (groundPlane.Raycast(cameraRay, out rayLenght))
            {

                Vector3 pointToLook = cameraRay.GetPoint(rayLenght);
                Debug.DrawLine(cameraRay.origin, pointToLook, Color.blue);

                transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z));
            }

        }
    }
}
