using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialWinCondition : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(GameObject.FindGameObjectWithTag("Enemy") == null)
        {
            Invoke("YouWin", 0.7f);
        }

    }
    void YouWin()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("WinScene");
    }
}
