using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    private List<string> sceneHistory = new List<string>();
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }


    public void ChooseMap()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("ChooseMapV2");
    }


    public void loadPlayground()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Demo");
    }

        public void loadTutorial()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Tutorial");
    }

        public void loadDesert()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Level2");
    }
    
            public void MainMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenuV2");
    }




    public void quitgame()
    {
        Application.Quit();
    }
}

