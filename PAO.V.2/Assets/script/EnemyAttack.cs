using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;


public class EnemyAttack : MonoBehaviour
{
    public ParticleSystem DestroyEffect;
    
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            SpawnDestroyEffect();
            Destroy(other.gameObject);
            Invoke("YouLose", 0.7f);

        }
        else if(other.tag == "Bullet")
        {
            SpawnDestroyEffect();
            Destroy(gameObject);
        }
        }
    private void SpawnDestroyEffect()
    {
        Vector3 brickPos= gameObject.transform.position;
        Vector3 spawnPosition = new Vector3(brickPos.x, brickPos.y, brickPos.z - 0.2f);
        GameObject effect = Instantiate(DestroyEffect.gameObject, spawnPosition, Quaternion.identity);
        Destroy(effect, DestroyEffect.main.startLifetime.constant);
    }

    void YouLose()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("LossScene");
    }

}
