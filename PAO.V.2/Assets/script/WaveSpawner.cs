using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WaveSpawner : MonoBehaviour {

    [SerializeField] TextMeshProUGUI customText;
    [SerializeField] TextMeshProUGUI uno;

    public enum SpawnState { SPAWNING, WAITING, COUNTING };

    [System.Serializable]
    public class Wave {
        public string name;
        public Transform enemy;
        public Transform secondEnemy;
        public int count;
        public float rate;
    }



    public Wave[] waves;
    private int nextWave = 0;
    private int currentWave = 0;

    public Transform[] SpawnPoints;

    public float timeBetweenWaves = 5f;
    public float waveCountdown = 0f;

    private float searchCountdown = 1f;

    private SpawnState state = SpawnState.COUNTING;

    void Start() {

        waveCountdown = timeBetweenWaves;

        Debug.Log(waves.Length);

    }

    void Update() {

        if (state == SpawnState.WAITING) {

            // Check if enemies are still alive
            if (EnemyIsAlive() == false)
            {
                
            
                WaveCompleted();

            }
            else
            {
                return;
            }

        }

        if (waveCountdown <= 0) {
            if (state != SpawnState.SPAWNING) {

                if (nextWave < waves.Length)
                {
                    StartCoroutine(SpawnWave(waves[nextWave]));
                }
            }
        }
        else
        {

            waveCountdown -= Time.deltaTime;

            if (waveCountdown >= 0 && nextWave < waves.Length)
            {
                customText.text = "Nauja banga už " + Mathf.Round(waveCountdown);
                currentWave = nextWave + 1;
                uno.text = "Bangos " + currentWave + "/" + waves.Length;
            }
            else
            {
                customText.text = "Arbūzai puola!";
            }

            if (nextWave >= waves.Length)
            {
                customText.text = "Sveikinimai! Okupacija sustabdyta!";
                uno.text = " ";
                Invoke("YouWin", 0.7f);
            }
        }
    }


    void WaveCompleted(){

        customText.text = "Wave Completed";

        Debug.Log("Wave Completed!");

        state = SpawnState.COUNTING;
        waveCountdown = timeBetweenWaves;

        if(nextWave >= waves.Length )
        {

            Debug.Log("ALL WAVES COMPLETED. Loopping");

            //RestartGame();
        }
        else
        {
            nextWave++;
        }
        Debug.Log(nextWave);
        


    }

    bool EnemyIsAlive() {

        searchCountdown -= Time.deltaTime;

        if (searchCountdown <= 0f)
        {
            searchCountdown = 1f;
            if (GameObject.FindGameObjectWithTag("Enemy") == null)
            {

                return false;
            }
        }
        return true;
   
    }

    IEnumerator SpawnWave(Wave _wave) {

        Debug.Log("Spawning Wave: " + _wave.name);
        state = SpawnState.SPAWNING;

        // Spawn

        for (int i = 0; i < _wave.count; i++) {

            SpawnEnemy(_wave.enemy);
            SpawnEnemy(_wave.secondEnemy);
            yield return new WaitForSeconds(1f / _wave.rate);
        }

        state = SpawnState.WAITING;

        yield break;
    }


    void SpawnEnemy(Transform _enemy) {

        //Spawn enemy
        Debug.Log("Spawning Enemy: " + _enemy.name);
        //Instantiate(_enemy, transform.position, transform.rotation);

        Transform _sp = SpawnPoints[Random.Range(0, SpawnPoints.Length)];

        Instantiate(_enemy, _sp.position, _sp.rotation);
        //Instantiate(_enemy, new Vector3(-25, 0, 0), Quaternion.identity);
    }

    void YouWin()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("WinScene");
    }

}
